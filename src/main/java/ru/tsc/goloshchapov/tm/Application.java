package ru.tsc.goloshchapov.tm;

import ru.tsc.goloshchapov.tm.api.ICommandRepository;
import ru.tsc.goloshchapov.tm.constant.ArgumentConst;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.model.Command;
import ru.tsc.goloshchapov.tm.repository.CommandRepository;
import ru.tsc.goloshchapov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        showWelcome();
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            showEnterCommand();
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void exit() {
        System.exit(0);
    }

    private static void showErrorCommand() {
        System.err.println("Error! Command not found");
    }

    private static void showErrorArgument() {
        System.err.println("Error! Argument not supported");
        System.exit(1);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showWelcome() {
        System.out.println("\n** WELCOME TO TASK MANAGER **");
    }

    public static void showEnterCommand() {
        System.out.println("\nENTER COMMAND:");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Vladimir Goloshchapov");
        System.out.println("E-MAIL: goloschapov@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

}
