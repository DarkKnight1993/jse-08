package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.ICommandRepository;
import ru.tsc.goloshchapov.tm.constant.ArgumentConst;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display help list of possible app commands"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display list of arguments"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display list of commands"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
